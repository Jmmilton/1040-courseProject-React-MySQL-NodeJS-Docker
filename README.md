**_ http://portfolio-1040.jasonmilton.me/ _**

to use app

Run npm i

in one terminal in the root of /backend, run 'docker compose build' then 'docker compose up'. And in another terminal in the root of /frontend, run 'npm start'
then view project at localhost:3000

Login User and password is saved in DB - sent to instructor

make sure to have .env file in /backend with:

PORT="port_number"
JWT_SECRET="JWT_secret"
DATABASE_PASSWORD='db_password'
DATABASE_NAME="db_name"
DATABASE_HOST="db_host_name"
DATABASE_USER="db_username"
DATABASE_ROOT_PASSWORD="db_root_user"

for mysql statements - refer to the mysql Dump file included in /backend
