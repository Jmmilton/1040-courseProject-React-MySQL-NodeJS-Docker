import mysql from 'mysql'
// import dotenv from 'dotenv'
// dotenv.config();

const connection = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
}
if (process.env.DATABASE_SOCKET) {
    connection.socketPath = process.env.DATABASE_SOCKET
} else {
    connection.host = process.env.DATABASE_HOST
}
const conn = mysql.createConnection(connection)

conn.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack)
        return
    }
   
    console.log('Database Connected')
})

export default conn